new Dialog({
    title: `Token Vision Configuration`,
    content: `
    <form>
        <div class="form-group">
            <label>Light Source:</label>
            <select id="light-source" name="light-source">
                <option value="aucun">None</option>
                <option value="torche">Torch</option>
                <option value="lanterne">Lantern</option>
                <option value="lightcantrip">LightCantrip</option>
            </select>
        </div>
    </form>
    `,
    buttons: {
        yes: {
          icon: "<i class='fas fa-check'></i>",
          label: `Apply Changes`,
          callback: () => applyChanges = true
        },
        no: {
          icon: "<i class='fas fa-times'></i>",
          label: `Cancel Changes`
        },
      },
    default: "yes",
    
    close: html => {
        if (applyChanges) {
            for ( let token of canvas.tokens.controlled ) {
                let lightSource = html.find('[name="light-source"]')[0].value || "aucun";

                switch (lightSource) {
                    case "aucun":
                        token.document.update({
                            light: {
                                bright: 0,
                                dim: 0
                                }
                        });
                        break;
                    case "torche":
                        token.document.update({
                            light: {
                               bright: 20,
                               dim: 40,
                               color: '#ec8c32',
                               gradual: false, 
                               alpha: 0.55,
                               animation: {
                                  type: 'torch',
                                  speed: 2,
                                  intensity: 1
                               }
                            }
                         });
                         break;
                    case "lanterne":
                        token.document.update({
                            light: {
                               bright: 30,
                               dim: 60,
                               color: '#ffe666',
                               gradual: false, 
                               alpha: 0.25,
                               animation: {
                                  type: 'torch',
                                  speed: 2,
                                  intensity: 1
                               }
                            }
                         });
                         break;
                    case "lightcantrip":
                        token.document.update({
                            light: {
                               bright: 20,
                               dim: 40,
                               color: '#ffe666',
                               gradual: false, 
                               alpha: 0.25,
                               animation: {
                                  type: 'none'
                               }
                            }
                         });
                         break;
                    case "default":
                        break;
                }
            }
        }
    }
}).render(true);